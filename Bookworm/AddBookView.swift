//
//  AddBookView.swift
//  Bookworm
//
//  Created by Admin on 06.03.2022.
//

import SwiftUI

struct AddBookView: View {
    @Environment(\.managedObjectContext) var moc
    @Environment(\.dismiss) var dismiss
    
    @State private var titile = ""
    @State private var author = ""
    @State private var rating = 3
    @State private var genre = ""
    @State private var review = ""
    
    @State private var showingFieldsAlert = false
    
    let genres = ["Fantasy", "Horror", "Kids", "Mystery", "Poetry", "Romance", "Thriller"]
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Name of book", text: $titile)
                    TextField("Author's name", text: $author)
                    
                    Picker("Genre", selection: $genre) {
                        ForEach(genres, id: \.self) {
                            Text($0)
                        }
                    }
                }
                Section {
                    TextEditor(text: $review)
                    
                    RatingView(rating: $rating)
                } header: {
                    Text("Write a review")
                }
                
                Section {
                    Button("Save") {
                        if isThereEmptyFields() {
                            showingFieldsAlert = true
                        } else {
                            let newBook = Book(context: moc)
                            newBook.id = UUID()
                            newBook.title = titile
                            newBook.author = author
                            newBook.rating = Int16(rating)
                            newBook.genre = genre
                            newBook.review = review
                            newBook.date = "\(Date.now.formatted(date: .long, time: .shortened))"
                            
                            try? moc.save()
                            dismiss()
                        }
                    }
                }
            }
            .navigationTitle("Add Book")
            .alert("Error", isPresented: $showingFieldsAlert) {
                Button("Cancel") { }
            } message: {
                Text("Please, enter a title and author")
            }
        }
    }
    func isThereEmptyFields() -> Bool {
        if titile.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || author.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            return true
        }
        return false
        
    }
    
    struct AddBookView_Previews: PreviewProvider {
        static var previews: some View {
            AddBookView()
        }
    }
}
