# Bookworm
This is an app to track which books you’ve read and what you thought of them
## Used features
- Using CoreData
- Using @Binding
## App's overview
![screen-gif](./Bookworm/BookWorm.gif)
